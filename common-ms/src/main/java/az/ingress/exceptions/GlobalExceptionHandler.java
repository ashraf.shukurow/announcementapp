package az.ingress.exceptions;

import az.ingress.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler({NotFoundException.class,AlreadyExistsException.class,NotAuthenticatedException.class})
    public ResponseEntity<ErrorResponse> handleException(Exception e){
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.setErrorMessage(e.getMessage());
        errorResponse.setDate(LocalDateTime.now());
        errorResponse.setErrorCode(getHttpStatus(e).value());
        errorResponse.setStatus(getHttpStatus(e));
        return ResponseEntity.status(errorResponse.getStatus()).body(errorResponse);
    }


    private HttpStatus getHttpStatus(Exception ex) {
        if (ex instanceof NotFoundException) {
            return HttpStatus.NOT_FOUND;
        }
        if(ex instanceof AlreadyExistsException){
            return HttpStatus.CONFLICT;
        }
        if (ex instanceof NotAuthenticatedException){
            return HttpStatus.UNAUTHORIZED;
        }
         else {
            return HttpStatus.INTERNAL_SERVER_ERROR;
        }

    }
}
