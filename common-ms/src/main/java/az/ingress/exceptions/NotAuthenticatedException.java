package az.ingress.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class NotAuthenticatedException extends RuntimeException {
    private String message;

}
