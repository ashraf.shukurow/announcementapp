package az.ingress.exceptions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class AlreadyExistsException extends RuntimeException {
    private String message;
}
