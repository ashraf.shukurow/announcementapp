package az.ingress.dto;

import lombok.Data;

@Data
public class TokenResponse {
    private String accessToken;
}
