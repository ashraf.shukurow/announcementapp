package az.ingress.mapper;

import az.ingress.dto.request.AnnouncementRequest;
import az.ingress.dto.response.AnnouncementDetailsResponse;
import az.ingress.entity.Announcement;
import az.ingress.entity.AnnouncementDetails;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AnnouncementDetailsMapper {
    AnnouncementDetails requestToEntity(AnnouncementRequest announcementRequest);
    AnnouncementDetailsResponse entityToResponse(Announcement announcement);

}
