package az.ingress.mapper;

import az.ingress.dto.request.AnnouncementRequest;
import az.ingress.dto.response.AnnouncementResponse;
import az.ingress.entity.Announcement;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface AnnouncementMapper {

    Announcement requestToEntity(AnnouncementRequest announcementRequest);
    @Mapping(source ="user",target = "userResponse")
    AnnouncementResponse entityToResponse(Announcement announcement);
}
