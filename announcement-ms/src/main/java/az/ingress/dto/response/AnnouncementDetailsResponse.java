package az.ingress.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnnouncementDetailsResponse {
    private Long id;
    private String title;
    private String description;
    private Double price;
    private LocalDate createdDate;
    private LocalDate updatedDate;
}
