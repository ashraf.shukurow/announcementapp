package az.ingress.entity;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDate;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class AnnouncementDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String title;
    private String description;
    private Double price;
    private LocalDate createdDate;
    private LocalDate updatedDate;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "announcement_id",referencedColumnName = "id")
    private Announcement announcement;
}
