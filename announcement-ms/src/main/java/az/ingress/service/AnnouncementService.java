package az.ingress.service;

import az.ingress.dto.request.AnnouncementRequest;
import az.ingress.dto.request.PageDto;
import az.ingress.dto.request.SearchCriteria;
import az.ingress.dto.response.AnnouncementResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AnnouncementService {

    Page<AnnouncementResponse> getAllAnnouncement(List<SearchCriteria> searchCriteriaList, Pageable pageable);
    Page<AnnouncementResponse> getMostViewedAnnouncements(PageDto pageRequest);
    void createAnnouncement(AnnouncementRequest announcementRequest);
    void updateAnnouncement(Long id,AnnouncementRequest announcementRequest);
    void deleteAnnouncement(Long id);
    Page<AnnouncementResponse> getAllOwnAnnouncement(Long userId, Pageable pageable);
    AnnouncementResponse getOwnAnnouncementWithId(Long id,Long userId);
    AnnouncementResponse getOwnMostViewedAnnouncement(Long userId);
}
