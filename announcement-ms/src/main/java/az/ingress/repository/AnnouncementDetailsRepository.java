package az.ingress.repository;

import az.ingress.entity.AnnouncementDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnnouncementDetailsRepository extends JpaRepository<AnnouncementDetails,Long> {
}
