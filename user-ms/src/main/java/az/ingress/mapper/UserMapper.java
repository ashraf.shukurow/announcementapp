package az.ingress.mapper;

import az.ingress.dto.request.UserRequest;
import az.ingress.dto.response.UserResponse;
import az.ingress.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    User requestToEntity(UserRequest userRequest);
    UserResponse entityToResponse(User user);
}
