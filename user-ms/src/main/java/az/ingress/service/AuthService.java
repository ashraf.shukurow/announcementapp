package az.ingress.service;


import az.ingress.dto.TokenResponse;
import az.ingress.dto.request.LoginRequest;
import az.ingress.dto.request.UserRequest;
import az.ingress.dto.response.UserResponse;

public interface AuthService {
    TokenResponse login(LoginRequest loginRequest);
    UserResponse register(UserRequest userRequest);
}
